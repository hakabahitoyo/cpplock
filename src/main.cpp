#include <iostream>
#include <string>

#include "cpplock.h"


using namespace std;


int main (int argc, char ** argv)
{
	string filename {"/var/www/html/index.html"};

	{
		cpplock::ReadLock readlock (filename);
	}

	{
		cpplock::WriteLock writelock (filename);
	}

	return 0;
}


